#include <stdio.h>
#include <stdlib.h>

#include "decbin.h"



int malloced = 0;
int freed = 0;

struct bits* dec_to_bin(int dec) {
	int rem_val;

	struct bits *pbit = NULL;
	while(dec > 0) {
		if (pbit == NULL) {	// Begin conversion
			pbit = (struct bits*) malloc(sizeof(struct bits));
			malloced += 1;
			pbit->next = NULL;
		} else {	// Make space for incoming bit
			/* Initiate new bit */
			struct bits *npbit = (struct bits*) malloc(sizeof(struct bits));
			malloced += 1;
			npbit->next = NULL;

			/* Append lower bit */
			npbit->next = pbit;

			/* Point to new bit */
			pbit = npbit;
		}

		rem_val = dec%2;	// Evaluate remainder

		pbit->bit = rem_val;
		dec = dec/2;	
		
	}

	return pbit;

}

void free_bits(struct bits *pbits) {
	struct bits* temp_pbits;
	while(pbits != NULL) {
		temp_pbits = pbits;
		pbits = pbits->next;
		free(temp_pbits);
		freed += 1;
	}
}

int main(void)
{
	puts("Welcome to Base Converter");
	puts("Please enter the decimal value you want converted to binary");
	int value;
	scanf("%d", &value);	

	struct bits *bit_val = dec_to_bin(value);
	puts("This is the converted decimal to binary");

	struct bits *beg_bit_val = bit_val; // Save beginning to free later
	while(bit_val != NULL) {
		printf("%d", bit_val->bit);
		bit_val = bit_val->next;
	}
	printf("\n");

	free_bits(beg_bit_val);
	
}

